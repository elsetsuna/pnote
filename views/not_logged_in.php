<?php
include "header.php";
?>
<style>
.mag {
  margin-top: 10%;
}
body {
    background: transparent url("images/bgr.jpg") repeat scroll 0% 0%;
} 
</style>
<body>
        <?php
// show potential errors / feedback (from login object)
if (isset($login)) {
    if ($login->errors) {
        foreach ($login->errors as $error) {
            $err =  $error;
        }
    }
    if ($login->messages) {
        foreach ($login->messages as $message) {
            $mes =  $message;
        }
    }
}
?>
<div class="mag">

<div class="col-md-4 col-md-offset-4">
              <!-- Horizontal Form -->
              <div class="box box-primary">
                <div class="box-header with-border" align="center">
                <img class="img-responsive" src="adminlogo.gif"></img>
<?php
                  if(isset($message)){
                    echo $message; }
                    if(isset($error)){
                      echo $error;
                    }
                  ?>
                </div><!-- /.box-header -->
                <!-- form start -->
                  <div class="box-body">
                  <form class="form-horizontal" method="post" action="index.php" name="loginform">
                      <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="login_input_username" name="user_name" placeholder="Username">
                      </div>
                      <br><br>
                      <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                      <div class="col-sm-10">
                        <input type="password" class="form-control" id="login_input_password" name="user_password" placeholder="Password">
                      </div>
                                    
                  </div><!-- /.box-body -->
                  <div class="box-footer" align="center">
                    <button type="submit" class="btn btn-primary btn-block" name="login">Sign in</button>
                    </form><br>
                    <font color="grey"><?php echo $CONFIG['Footer_Copy']; ?></font>
                  </div><!-- /.box-footer -->
                  
              </div><!-- /.box -->
    </div>
    </div>
<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/bootstrap.js"></script>

<script src="js/signin.js"></script>

</body>

</html>
