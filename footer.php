
    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <script src="js/plugins/dataTables/datatables.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- jQuery UI -->
    <script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>
        <!-- Page-Level Scripts -->
          <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea' });</script>
  
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 10,
                responsive: true,
            });

        });
        $(document).ready(function(){
            $('.data4d').DataTable({
                pageLength: 10,
                responsive: true,
                ordering:  false,

            });

        });
                $(document).ready(function(){
            $('.registeredmember').DataTable({
                pageLength: 25,
                responsive: true,
                ordering:  false,

            });

        });
                                $(document).ready(function(){
            $('.listangkamenang').DataTable({
                pageLength: 10,
                responsive: true,
                ordering:  false,

            });

        });
    </script>
</body>