 <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="../img/profile_small.jpg" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">David Williams</strong>
                             </span> <span class="text-muted text-xs block">Art Director <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="profile.html">Profile</a></li>
                            <li><a href="contacts.html">Contacts</a></li>
                            <li><a href="mailbox.html">Mailbox</a></li>
                            <li class="divider"></li>
                            <li><a href="login.html">Logout</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>
                <li >
                    <a href="index.php?page=inputpemenang"><i class="fa fa-pencil-square-o"></i> <span class="nav-label">Input Angka Menang</span></a>
                </li>
                <li>
                    <a href="index.php?page=listangkamenang"><i class="fa fa-list-alt"></i> <span class="nav-label">History Angka Menang</span></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-ticket"></i> <span class="nav-label">History Entry Player</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="index.php?page=history4d">History Entry 4D</a></li>
                        <li><a href="index.php?page=history3d">History Entry 3D</a></li>
                        <li><a href="index.php?page=history2d">History Entry 2D</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-envelope"></i> <span class="nav-label">List Pemenang </span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="index.php?page=listpemenang4d">List Pemenang 4D</a></li>
                        <li><a href="index.php?page=listpemenang3d">List Pemenang 3D</a></li>
                        <li><a href="index.php?page=listpemenang2d">List Pemenang 2D</a></li>
                    </ul>
                </li>
                               <li>
                    <a href="index.php?page=editberita"><i class="fa fa-pencil"></i> <span class="nav-label">Edit Halaman</span></a>
                </li>
                                               <li>
                    <a href="index.php?page=registeruser"><i class="fa fa-list-alt"></i> <span class="nav-label">Registered User</span></a>
                </li>
                                                               <li>
                    <a href="index.php?page=tabelklasemen"><i class="fa fa-trophy"></i> <span class="nav-label">Tabel Kelasemen</span></a>
                </li>
            </ul>

        </div>
    </nav>