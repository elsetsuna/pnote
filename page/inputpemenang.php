<?php
if(isset($_POST['submitsg'])){
    $entry = $_POST['angkasg'];
$d4 = get4d($entry);
$d3 = get3d($entry);
$d2 = get2d($entry);
savesg($d4,$d3,$d2);
getmenangsg($d2,$d3,$d4);
}

if(isset($_POST['submithk'])){
    $entry = $_POST['angkahk'];
$d4 = get4d($entry);
$d3 = get3d($entry);
$d2 = get2d($entry);
savehk($d4,$d3,$d2);
getmenanghk($d2,$d3,$d4);
}

if(isset($_POST['submitpt'])){
    $entry = $_POST['angkapt'];
$d4 = get4d($entry);
$d3 = get3d($entry);
$d2 = get2d($entry);
savept($d4,$d3,$d2);
getmenangpt($d2,$d3,$d4);
}

if(isset($_POST['submitlv'])){
    $entry = $_POST['angkalv'];
$d4 = get4d($entry);
$d3 = get3d($entry);
$d2 = get2d($entry);
savelv($d4,$d3,$d2);
getmenanglv($d2,$d3,$d4);
}
?>
<?php
if(isset($msg)){
    echo $msg;
}
?>
<div class="row">
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Input Pemenang Singapore</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#">Config option 1</a>
                        </li>
                        <li><a href="#">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form class="form-inline" target="" action="" method="post">
                    <div class="form-group">
                        <input type="text" placeholder="Input Angka"
                        class="form-control col-md-12" name="angkasg">
                    </div>
                    <br><br>
                    <button class="btn btn-primary" type="submit" name="submitsg" disabled>Simpan</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Input Pemenang Hongkong</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#">Config option 1</a>
                        </li>
                        <li><a href="#">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form class="form-inline" target="" action="" method="post">
                    <div class="form-group">
                        <input type="text" placeholder="Input Angka"
                        class="form-control col-md-12" name="angkahk">
                    </div>
                    <br><br>
                    <button class="btn btn-warning" type="submit" name="submithk">Simpan</button>
                </form>
            </div>
        </div>
    </div>

    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Input Pemenang Liverpool</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#">Config option 1</a>
                        </li>
                        <li><a href="#">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form class="form-inline" target="" action="" method="post">
                    <div class="form-group">
                        <input type="text" placeholder="Input Angka"
                        class="form-control col-md-12" name="angkalv">
                    </div>
                    <br><br>
                    <button class="btn btn-danger" type="submit" name="submitlv">Simpan</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Input Pemenang Pattaya</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#">Config option 1</a>
                        </li>
                        <li><a href="#">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form class="form-inline" target="" action="" method="post">
                    <div class="form-group">
                        <input type="text" placeholder="Input Angka"
                        class="form-control col-md-12" name="angkapt">
                    </div>
                    <br><br>
                    <button class="btn btn-success" type="submit" name="submitpt" disabled>Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>